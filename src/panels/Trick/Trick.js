import React, {Fragment, useContext } from 'react';
import {
  Button,
  Cell,
  Div,
  FixedLayout,
  Group,
  Header,
  PanelHeaderBack,
  PanelHeaderSimple,
} from "@vkontakte/vkui";

import Context from "../../components/App/context";
import TrickPic from "../../components/TrickPic/TrickPic";
import HardLevel from "../../components/HardLevel/HardLevel";
import {useRoute } from "react-router5";
import {share} from "../../actions/functions";
import TrickProgressBtnList from "../../components/TrickProgressBtnList/TrickProgressBtnList";

const Trick = () => {
  const {trickProgress, trickList, activeTrickSnackbar} = useContext(Context);

  const {route: {params: {trickId}}} = useRoute();
  const trick = trickList.find(({id}) => id === trickId) || {};
  const trickProgressItem = trickProgress.find(({id}) => id === trickId) || {id: trickId, progress: 1};

  const goToHome = () => window.history.back();

  return (
    <Fragment>
      <PanelHeaderSimple
        left={<PanelHeaderBack onClick={goToHome}/>}>{trick.name || "Трюк"}</PanelHeaderSimple>
      <Div style={{ height: '90vw'}}>
        <TrickPic id={trickId || 'def'}/>
      </Div>
      <Group header={<Header mode="secondary">Уровень</Header>}>
        <Cell>
          <HardLevel level={trick.level || 1}/>
        </Cell>
      </Group>
      <Group style={{ paddingBottom: 60}} header={<Header mode="secondary">Описание</Header>}>
        <Cell multiline={true}>
          {trick.desc || ''}
        </Cell>
        <Cell><Button size="m" onClick={() => share()} mode="commerce">Поделиться</Button></Cell>
      </Group>
      <FixedLayout vertical="bottom">
        <TrickProgressBtnList id={trick.id} progress={trickProgressItem.progress}/>
      </FixedLayout>
      {activeTrickSnackbar}
    </Fragment>
  )
};

export default Trick;

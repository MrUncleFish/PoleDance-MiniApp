import React from 'react';
import PropTypes from "prop-types";
import {getImg} from "../../actions/functions";

const TrickPic = ({ id }) => {

  if (id === 'def') return (<div style={{width: '100%', height: '30vh', background: 'white'}}/>);
  return (<img style={{width: '100%'}} alt="" src={getImg(id)}/>);
};

TrickPic.propTypes = {
  id: PropTypes.string.isRequired,
};

export default TrickPic;
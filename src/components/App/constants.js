export const APP_ID = 7382236;
export const IMG_PATH = 'https://keylordworkshop.ru/poledance/';

export const TRICK_LIST = [
  {
    id: '1',
    name: 'Бильман',
    alt_name: '',
    eng_name: 'Bielmann',
    level: 2,
    desc: 'Необходимо стоять на одной ноге (можно прислониться к стене или к шесту, чтобы не терять равновесие). ' +
      'Далее рукой беремся за свободную ногу, вытягиваем колено, сгибаемся в пояснице, фиксируем данное положение',
  },
  {
    id: '2',
    name: 'Бутон',
    alt_name: '',
    eng_name: 'Flowerbud',
    level: 1,
    desc: 'Поднимитесь на пилон, отпустите одну руку, прогнитесь назад и постарайтесь взять пилон свободной рукой. ' +
      'Отталкивайтесь опорной ногой от пилона, попробуйте снять вторую руку и захватить ею пилон рядом с первой. ' +
      'Затем аккуратно переставьте вторую ногу с передней стороны пилона назад, за первую ногу.',
  },
  {
    id: '3',
    name: 'Мост',
    alt_name: 'Банан, Полнолуние',
    eng_name: 'Bridge (Cross Ankle Release, Full moon)',
    level: 2,
    desc: 'Сначала нужно забраться на середину шеста и обхватить его ногами (шест – посередине). ' +
      'С прямыми напряженными ногами и натянутыми носочками медленно опускаем корпус вниз. ' +
      'После этого руками хватаемся за шест и поднимаем их как можно выше, при этом тянутся ' +
      'плечи и грудь. Фиксируем данное положение',
  },
  {
    id: '4',
    name: 'Оттяжка в китайском шпагате',
    alt_name: '',
    eng_name: '',
    level: 3,
    desc: 'Сначала выполнить элемент "Китайский шпагат", для этого упираясь передней ногой в пилон на уровне таза, ' +
      'держась двумя руками за пилон, приподнять заднюю ногу, отведя ее назад, передняя нога выпрямляется, стопа упирается на пилон.' +
      ' Далее левой рукой (противоположная передней ноге) захватить заднюю ногу и оттянуть её назад как можно больше раскрывая продольный шпагат',
  },
  {
    id: '5',
    name: 'Рация',
    alt_name: '',
    eng_name: '',
    level: 2,
    desc: 'необходимо сделать "Вис на дальней ноге", не отпуская рук, затем дальней рукой внизу взяться за пилон ' +
      'кольцом вверх или упором в основание ладони. Свободную ногу завести за пилон так, чтобы образовать разножку',
  },
  {
    id: '6',
    name: 'Задний планш',
    alt_name: '',
    eng_name: 'Planch',
    level: 3,
    desc: 'Залезаем на пилон любым удобным способом. Наклоняем корпус вперёд, обе руки ставим на пилон «кольцом вверх», ' +
      'дальняя от пилона рука находится снизу. По очереди вытягиваем ноги назад, корпус вместе с ногами должен быть чётко параллельно полу.',
  },
  {
    id: '7',
    name: 'Вис Варды',
    alt_name: '',
    eng_name: 'Varda',
    level: 2,
    desc: 'Выходы в этот элемент возможны разные, самый простой осуществляется из четверки. После того, ' +
      'как вы повисли в четверке, необходимо руку, которая является одноименной с верхней ногой, поставить под ' +
      'спину на пилон кольцом (т.е. большим пальцем) наверх. Вторая рука опускается прямой на пилон кольцом вниз так, ' +
      'чтобы спина «ложилась» на пилон. В этот момент необходимо зацепиться за пилон носочком верхней ноги и опустить ' +
      'вторую ногу вниз, развернув корпус в пол. Если очень не хватает растяжки, чтобы вытянуть ногу в шпагат, то элемент ' +
      'будет красивее смотреться в полушпагате с согнутой свободной ногой.',
  },
  {
    id: '8',
    name: 'Старфиш',
    alt_name: 'Морская звезда',
    eng_name: 'Star fish',
    level: 1,
    desc: 'Для начала следует научиться выполнять элемент с пола. Упритесь дальней ногой в основание пилона и ' +
      'зацепите носочком ближней ноги пилон примерно на уровне головы. Колено верхней ноги стоит развернуть к потолку, ' +
      'чтобы увеличить сцепление. Если вы чувствуете, что держитесь, отпустите руки: сначала нижнюю, затем верхнюю. ' +
      'Важно: Элемент довольно сложен и болезнен, поэтому стоит его тренировать, когда уровень вашего пилонного мастерства ' +
      'уже достаточно высок. Выполняя трюк не с пола, а с пилона, обязательно постелите мат или попросите друга подстраховать вас.',
  },
  {
    id: '9',
    name: 'Флаг',
    alt_name: '',
    eng_name: '',
    level: 2,
    desc: 'Из элемента "Шолдер маунт", держась одной ногой за пилон под коленом или стопой ноги, опустить нижнюю руку ' +
      'в упор на пилон, отводя голову и корпус от пилона, образуя уголок',
  },
  {
    id: '10',
    name: 'Аиша',
    alt_name: '',
    eng_name: 'Ayesha',
    level: 3,
    desc: 'Разновидность "флага". Необходимо выйти в «Рогатку», затем в «Пожарника», ближнюю руку опустить вниз с присогнутым локтем ' +
      'и вытолкнуть себя в «Гусеничку» (таз оторван от пилона), затем второй рукой цепляемся локтем за пилон перпендикулярно ему, ' +
      'немного отводим таз назад и раскрываем ноги',
  },
  {
    id: '11',
    name: 'Карандаш',
    alt_name: '',
    eng_name: 'Pencil',
    level: 2,
    desc: 'Для выполнения элемента нужно сначала выйти в V-флаг. После этого выпрямляется корпус и подтягиваются ноги, ягодицы и живот до ровной линии.',
  },
  {
    id: '12',
    name: 'Затяжка из виса на ближней ноге',
    alt_name: '',
    eng_name: '',
    level: 2,
    desc: 'Сначала нужно выполнить «Вис на ближней ноге». Прогнуться в спине и захватить свободную ногу одноимённой рукой',
  },
  {
    id: '13',
    name: 'Паук',
    alt_name: 'Наездница',
    eng_name: 'Horsewoman',
    level: 1,
    desc: 'Выполняется из виса на дальней ноге. Далее вам следует подтянуться наверх и перекинуть вторую ногу перед пилоном, зацепившись ею под колено',
  },
  {
    id: '14',
    name: 'Крыло бабочки',
    alt_name: '',
    eng_name: 'Butterfly Wing',
    level: 1,
    desc: 'Заходим в «Рогатку». Делаем зацеп на дальнюю ногу. Ставим нижнюю руку в упор, верхнюю выворотным хватом. ' +
      'Теперь мы находимся в элементе «Скорпион». Отталкиваем себя от пилона, за счет этого выпрямляем ногу, которая была в зацепе, ' +
      'выпрямили и цепляемся стопой. Тело разворачиваем параллельно полу (визуально положение как во флажке). Вторая нога может быть ' +
      'согнута или выпрямлена. В данном элементе работают мышцы предплечий, спины,  лучезапястный сустав',
  },
  {
    id: '15',
    name: 'Радуга Марченко',
    alt_name: '',
    eng_name: '',
    level: 3,
    desc: 'Забраться на середину шеста. После уйти в «Банан», затем подмышкой крепко ухватиться за шест. ' +
      'Далее хватаемся одной рукой за ногу, вытягиваем ее. Если чувствуете, что крепко держитесь, то можно опустить вторую ногу',
  },
  {
    id: '16',
    name: 'Мартини',
    alt_name: '',
    eng_name: 'Martini, Martini seat, Side elbow seat',
    level: 1,
    desc: 'Выходим в него из "Рогатки", далее делаем Вис на дальней ноге, поднимаемся наверх, переносим свободную ногу перед пилоном и зажимаем ее противоположной рукой',
  },
  {
    id: '17',
    name: 'Складка Феликс',
    alt_name: 'Курица на вертеле, Цыпленок на вертеле, Спэчкок, Арбалет',
    eng_name: 'Spatchcock',
    level: 3,
    desc: 'Один из возможных выходов в элемент начинается с «Шолдермаунта» с женским хватом. Поднимаете ножки наверх ' +
      'и цепляете пилон пяткой той ноги, которая находится у вас сверху, между руками. После этого переставляете нижнюю руку ' +
      'в упор, отталкиваясь от шеста, чтобы зацепить пилон другой ногой снизу. Переставьте руку спереди от пилона так, ' +
      'чтобы плечи лежали на шесте, и протолкните корпус дальше за пилон. Теперь можно отпустить руки.',
  },
  {
    id: '18',
    name: 'Санрайз',
    alt_name: '',
    eng_name: 'Sunrise',
    level: 2,
    desc: 'Чаще всего выполняется элемент из «наездницы» с выпрямлением верхней ноги и разворотом корпуса.',
  },
  {
    id: '19',
    name: 'Планер в шпагате',
    alt_name: '',
    eng_name: 'Glider Split',
    level: 2,
    desc: 'Элемент делается из рогатки, из виса на дальней ноге. Сначала делаем рогатку на правую сторону и ' +
      'выходим в вис на дальней ноге. В данном случае дальняя нога – левая. Правую руку оставляем на пилоне, ' +
      'а левую – выводим вниз и ставим в мужской хват. Далее правую ногу переводим вперед, как бы накатывая ногу ' +
      'на пилон на правую ногу. Затем снимаем с пилона левую и стараемся прогнуться в пояснице как можно сильнее. ' +
      'Это элемент для продвинутого уровня, для девушек, которые имеют уже хорошую растяжку. При ее отсутствии элемент ' +
      'не получится выполнить правильно. Поэтому нужно работать растяжение продольного шпагата.',
  },
  {
    id: '20',
    name: 'Русский шпагат',
    alt_name: '',
    eng_name: 'Russian Split',
    level: 3,
    desc: 'Для данного элемента нужно иметь не только хорошую растяжку, но и крепкие руки, чтобы удержать свое тело в ' +
      'горизонтальном положении и затем подтянуть себя назад к пилону. Более того, нужно следить, чтобы упорная нога не скользила',
  },
  {
    id: '21',
    name: 'Гемини',
    alt_name: 'Вис на ближней ноге',
    eng_name: 'Gemini, Inside leg hang',
    level: 1,
    desc: 'Необходимо закинуть ноги в «Рогатку» и захватить пилон под коленом ближней ноги. ' +
      'Возможны два варианта захвата: нога зажимает пилон только под коленом либо нога вытягивается вдоль пилона для лучшего сцепления',
  },
  {
    id: '22',
    name: 'Китайские палочки',
    alt_name: 'Палочки',
    eng_name: 'Split Chopsticks, Chopsticks',
    level: 2,
    desc: 'Поднимитесь на пилон и зажмите его между ногами, как в горизонте. Наклонитесь вперед в складочку к ногам и перехватите дальней рукой ' +
      'ближнюю ногу, крепко зажимая пилон между бедром и боком, ближняя рука при этом остается на пилоне. Переведите дальнюю ногу назад в шпагат параллельно полу.',
  },
  {
    id: '23',
    name: 'Жанейро',
    alt_name: 'Джанейро',
    eng_name: 'Janeiro',
    level: 3,
    desc: 'Исходное положение – брассманка, из которой корпус переходит в параллельное полу положение с упором на согнутую руку. ' +
      'Важно: вторая рука снимается с пилона. Выход требует тщательной отработки, ведь необходимо удержать тело в горизонтальном положении, ' +
      'фактически держась одной рукой',
  },
  {
    id: '24',
    name: 'Тюльпан',
    alt_name: '',
    eng_name: 'Tulip',
    level: 2,
    desc: 'Верхняя рука в ленточном хвате, нижняя в распоре, кольцом вниз. Требуется вымахнуть ногой от пилона и развернуть корпус ' +
      'грудью вверх, прогнуться в спине. Обе руки держать прямыми, ноги прямые вместе, либо согнуть в колене одну',
  },
  {
    id: '25',
    name: 'Лук и стрелы',
    alt_name: '',
    eng_name: 'Bow And Arrow',
    level: 3,
    desc: 'Делаем заход на пилон, далее нужно сесть, скрестить ноги вместе прямые или же согнуть одну сверху другой и ' +
      'опуститься вниз головой, держась руками за пилон под бедрами. Затем необходимо упереться плечом в пилон и ' +
      'зацепиться стопой любой ноги за пилон, а другую ногу отвести от пилона и согнуть в колене. Включены в работу мышцы рук и ног, ' +
      'желательно выучить подводящий элемент на полу, стоя на локтях',
  },
  {
    id: '26',
    name: 'Скорпион',
    alt_name: 'Бабочка, Свастика',
    eng_name: 'Butterfly',
    level: 1,
    desc: 'Трюк выполняется из виса на дальней ноге. Дальняя рука ставится на пилон около бедра кольцом вверх, либо выворотным хватом. ' +
      'Вторая рука ставится в распор, после этого необходимо развернуть корпус лицом к пилону',
  },
  {
    id: '27',
    name: 'Летучая мышь',
    alt_name: 'Жасмин',
    eng_name: 'Bat, Bird, Bird nest',
    level: 1,
    desc: 'Элемент несложный и может быть быстро освоен при свободном выполнении Виса на дальней ноге. Зажмите крепко шест под коленом. ' +
      'Положите носочек ближней ноги на носочек дальней, еще сильнее зажимая пилон, прогнитесь, толкая бедра вперед, и отпустите руки. ' +
      'На начальном этапе можно помогать себе держать зажим под коленом, держась руками за носочек дальней ноги.',
  },
  {
    id: '28',
    name: 'Супермен',
    alt_name: '',
    eng_name: 'Batman, Superman',
    level: 1,
    desc: 'Один из вариантов захода в элемент: сначала необходимо выполнить Вис на дальней ноге, затем ближнюю руку поставить вниз в упор,' +
      ' подняв корпус до параллели с полом. Ближнюю ногу прямую перевести к себе и вторую руку переставить над ногой в зацепе. Далее выполнить ' +
      'три действия одновременно: снять ногу с зацепа, развернуть корпус и убрать нижнюю руку с упора. Обе ноги выпрямить и сильно зажать пилон между ногами.',
  },
  {
    id: '29',
    name: 'Морской конек',
    alt_name: '',
    eng_name: 'Seahorse, Butt Hold',
    level: 3,
    desc: 'Зайти в вис на дальней ноге, одноименной рукой перехватиться сверху ноги за пилон, другой ногой захватить пилон снизу под коленом, ' +
      'выпрямить верхнюю ногу, развернувшись спиной к пилону, нижней рукой отпуская пилон. Затем, вывести верхнюю ногу вперёд положив сверху ' +
      'другой ноги, скрестив их «нога на ногу»',
  },
  {
    id: '30',
    name: 'Райская птица',
    alt_name: '',
    eng_name: 'Bird of paradise',
    level: 3,
    desc: 'Трюк выполняется из виса на дальней ноге. Необходимо развернуть корпус так, чтобы спина «легла» на шест. ' +
      'Затем нужно завести локоть дальней руки за пилон, при этом шея «обнимает» пилон с другой стороны. После ' +
      'подтяните ближней рукой ближнюю ногу, обнимите ее под локоть спереди и сцепите руки в замок. Только теперь ' +
      'можно снять дальнюю ногу с пилона и увести ее назад в шпагат.',
  },
  {
    id: '31',
    name: 'Птичка',
    alt_name: '',
    eng_name: 'Peter Pan sit, Dive sit, Swan variation',
    level: 1,
    desc: 'Для выхода в нее достаточно подтянуть себя руками вверх, обхватить пилон ногами и отклонить корпус в сторону, ' +
      'как бы «выглянув» из-за пилона. Благодаря наклону корпуса и положению бедер «одно поверх другого» ваши ноги не будут ' +
      'скользить по шесту. Теперь остается лишь обхватить пилон под коленом той ноги, в сторону которой отклонен корпус, и ' +
      'еще больше вывести корпус вперед перед пилоном.',
  },
  {
    id: '32',
    name: 'Лебедь',
    alt_name: '',
    eng_name: 'Swan',
    level: 1,
    desc: 'Для выполнения элемента необходимо согнуть ближнюю ногу в колене, обнять ее дальней рукой и ' +
      'зафиксировать руку на пилоне кольцом вверх на уровне бедра. Шест уводится под подмышку ближней руки, ' +
      'и этой рукой перехватывается дальняя нога, как на фото. Трюк несложный, также подходит для среднего уровня.',
  },
  {
    id: '33',
    name: 'Купидон',
    alt_name: 'Королевская креветка',
    eng_name: 'Cupid',
    level: 1,
    desc: 'Сначала делается оттяжка коленным захватом и упором стопой. Далее -  зацеп свободной стопой за одноимённый локоть, ' +
      'прогиб в боку, корпус направлен от пилона, и голова смотрит вверх',
  },
  {
    id: '34',
    name: 'Зонтик',
    alt_name: '',
    eng_name: 'Umbrella',
    level: 2,
    desc: 'Выполняется из «Шолдера». Необходимо выйти в «Шолдер маунт» женским хватом и зажать ноги в ' +
      '«Пожарнике», далее согнутую руку опустить вниз враспор, оставив упор в шею. Вторую руку увести ' +
      'под локоть. Ногами проскользнуть вдоль пилона вниз до вариации «Стульчика», отведя таз назад. ' +
      'Далее держа баланс, развести ноги как во «Флаг».',
  },
  {
    id: '35',
    name: 'Шолдермаунт',
    alt_name: 'Шолдер',
    eng_name: 'Air Shouldermount',
    level: 2,
    desc: 'Необходимо встать спиной к пилону, одной рукой взяться захватом «твист», вторую руку согнуть в локте ' +
      'и взяться открытым хватом (капом) за пилон. За счёт пресса поднять тело наверх как в «Рогатку» ' +
      'и развести ноги максимально широко, не уводя упора из-под плеча',
  },
  {
    id: '36',
    name: 'Распорка из рук',
    alt_name: '',
    eng_name: '',
    level: 1,
    desc: 'Встать лицом к пилону. Одной рукой захватить пилон выше уровня головы обычным хватом, ' +
      'другой рукой захватываем пилон ниже уровня таза пальцами вниз и упираемся в него ладонью. ' +
      'Отрываем обе ноги от пола, одну из ног сгибаем в колене, другую направляем назад',
  },
  {
    id: '37',
    name: 'Струнка',
    alt_name: '',
    eng_name: '',
    level: 1,
    desc: 'Рассмотрим вариацию с раскрытыми ножками. Необходимо залезть наверх любым способом. ' +
      'Выйти в сид «четвёрку», скосить таз в сторону согнутой верхней ноги, прогнуться в спине назад, ' +
      'затем раскрыть нижнюю ногу вниз',
  },
  {
    id: '38',
    name: 'Титаник',
    alt_name: '',
    eng_name: '',
    level: 2,
    desc: 'Данный элемент выполняется из элемента «Супермен», для этого необходимо выполнить выход в супермена, ' +
      'затем опустить вниз одноимённую ногу с той рукой, которая держится за пилон и поставить внутреннюю часть стопы на пилон, ' +
      'сильно давя ею в шест. Вторую ногу слегка опустить вниз сохраняя зацеп между коленями; подтянуть себя рукой, что держится ' +
      'за пилон и свободной рукой взяться за него сверху. Упереться в плечо и отпустить одну руку в сторону',
  },
  {
    id: '39',
    name: 'Голубь',
    alt_name: 'Дуга',
    eng_name: 'Dove, Half Moon',
    level: 1,
    desc: 'Зайдите в элемент «Супермен», максимально прогнитесь в спине и подтяните корпус наверх. ' +
      'Перехватите свободной рукой пилон «кольцом вниз», переставьте вторую руку в такое же положение. ' +
      'Медленно и постепенно выпрямите руки, пытаясь еще сильнее прогнуться в спине и груди, дотянуться затылком до пилона.',
  },
  {
    id: '40',
    name: 'Колечко',
    alt_name: '',
    eng_name: '',
    level: 2,
    desc: 'Сидя на пилоне, опуститься вниз головой. Под бедрами взяться двумя руками за пилон кольцом вверх, ' +
      'упереться плечом в пилон (каким более удобно, желательно чтобы одноименная рука была верхней на пилоне). ' +
      'Затем не спеша разжать ноги и согнуть их в коленях, бедра автоматически опустятся вниз. В этом элементе ' +
      'важную роль играет сила рук, поэтому необходимо укреплять хваты разными подводящими элементами',
  },
  {
    id: '41',
    name: 'Хвост дракона',
    alt_name: '',
    eng_name: 'Dragon\'s tail',
    level: 2,
    desc: 'Выполните вис на дальней ноге, переставьте ближнюю руку так, чтобы она была направлена вниз вдоль пилона, кольцом вниз. ' +
      'Отталкиваясь ближней рукой и прогибаясь в пояснице, заведите дальнюю руку за спину, поставив ее на пилон кольцом вверх. ' +
      'Переведите ближнюю ногу на себя в складочку, крепко прижимая ею пилон. Попробуйте снять дальнюю ногу с пилона и ' +
      'еще сильнее прогнуться назад. Чем ближе носочек дальней ноги к голове (в кольцо), тем красивее смотрится трюк.' +
      ' Возможны вариации выполнения «Хвоста дракона», когда обе ноги сгибаются в колене (как на фото), обе ноги распрямляются в ' +
      'шпагат (Dragon Split) и когда рука под спину ставится кольцом вниз.',
  },
  {
    id: '42',
    name: 'Рогатка',
    alt_name: 'Разножка',
    eng_name: 'Chopper, Open-V',
    level: 2,
    desc: 'Нужно встать  любым боком к пилону, взяться за него двумя руками, ближняя должна быть рука согнута в локте ниже дальней. ' +
      'Затем зажимаем пилон между ближней рукой и грудью. Дальняя рука полусогнута, расположена на пилоне на уровне лица. ' +
      'Ближней ногой делаем небольшой шаг вперёд, а дальней замах наверх, в это время нужно оттолкнуться сильно ближней ногой ' +
      'от пола и подтянуть бедра наверх. Затем зафиксировать таз наверху путём напряжения мышц живота, максимально приблизив его ' +
      'к пилону, ноги развести в стороны. В работу включены мышцы живота и рук',
  },
  {
    id: '43',
    name: 'Шпагат Джамилы',
    alt_name: '',
    eng_name: '',
    level: 2,
    desc: 'Для выполнения данного элемента надо закинуть ноги на шест, при этом натянуть колени, вытянуть спину и носочки. ' +
      'После одну ногу закинуть на шест, повиснуть на бочке, максимально прогнуться в спине, вытянуть вторую ногу. ' +
      'Далее прямую ногу развернуть в сторону корпуса, одной рукой тянуть ногу на себя, а второй придерживать ягодицы. ' +
      'После того, как поймали равновесие, ногу с шеста вытягиваем в противоположную сторону второй ноги',
  },
  {
    id: '44',
    name: 'Журавлик',
    alt_name: '',
    eng_name: 'Inverted Thigh Hold',
    level: 1,
    desc: 'Элемент выполняется из Виса на дальней ноге. Дальше ближнюю руку необходимо поставить вниз на пилон и ' +
      'довернуть корпус лицом к пилону, переведя ногу на себя и сильно прижав к пилону (грудь прижата к шесту). ' +
      'Вторую руку необходимо переставить вниз к первой и вытолкнуть себя от пилона одной рукой. Вторую отвести в сторону',
  },
  {
    id: '45',
    name: 'Пожарник',
    alt_name: '',
    eng_name: '',
    level: 1,
    desc: 'Держась двумя руками, захватить пилон между коленей и стоп, отвести таз назад на уровне коленей, убрать одну руку с пилона и развернуть корпус в сторону свободной руки',
  },
  {
    id: '46',
    name: 'Шпагат ALINEK',
    alt_name: '',
    eng_name: 'Aline K Split',
    level: 3,
    desc: 'Необходимо встать спиной к пилону и взяться за него руками, как в «шолдере» (мужским или женским хватом). ' +
      'Затем одна нога ставится в упор к пилону, другая выпрямляется вперед в шпагат.',
  },
  {
    id: '47',
    name: 'Лотос',
    alt_name: '',
    eng_name: 'Lotus, Knee Hold',
    level: 1,
    desc: 'Выход из «Лотоса» осуществляется через зацеп дальней ногой, далее необходимо отпустить руки',
  },
  {
    id: '48',
    name: 'Черри',
    alt_name: '',
    eng_name: '',
    level: 3,
    desc: 'Делаем "Вис на дальней ноге", прогибаемся в спине и выводим корпус перед пилоном, ближней рукой нужно притянуть свободную ногу к себе, ' +
      'постепенно закручиваясь в Колечко. Захват ноги производим изнутри, за колено. Внешней рукой притянуть себя за стопу. Голову просунуть через ' +
      'получившееся кольцо. Обратите внимание &#8211; корпус и голова должны быть развёрнуты от пилона. Внутреннюю руку можно выпрямить',
  },
  {
    id: '49',
    name: 'Х-Флаг',
    alt_name: '',
    eng_name: 'Iron X',
    level: 3,
    desc: 'Обе руки должны быть выпрямлены. Хват любой, кроме «чашечки». Обе ноги выпрямлены и держатся в позиции «рогатка», ' +
      'бедра в естественном положении относительно положения туловища, ноги держатся ровно. Все туловище (от головы до бёдер)' +
      ' находится под углом 90 градусов к пилону и смотрит в одно направление',
  },
  {
    id: '50',
    name: 'Брассманка',
    alt_name: 'Обезьянка',
    eng_name: 'Brass Monkey',
    level: 2,
    desc: 'Один из самых распространенных выходов в этот элемент – из «Четверки». Необходимо ближнюю руку, согнутую в локте, ' +
      'поставить кольцом вниз, схватившись под подмышкой, вторую руку поставить в упор. Дальней ногой зацепиться за пилон ' +
      'подъемом с прямым коленом, увести  таз немного в сторону и второй ногой зацепиться под коленом. ' +
      'Вторую ногу положить на первую сверху. Руки разводим в стороны.',
  },
  {
    id: '51',
    name: 'Стульчик',
    alt_name: '',
    eng_name: '',
    level: 1,
    desc: 'Выполняется из исходного положения – стоя боком к пилону. Держим руки в узком хвате (кисти на расстоянии предплечья) и ' +
      'делаем шаг внутренней ногой и замах внешней, складываем ноги в нужную форму. Фиксируем положение несколько секунд. Сходим на ровные ноги или в пол',
  }
];
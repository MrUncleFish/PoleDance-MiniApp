import React from 'react';
import {Avatar, Cell} from "@vkontakte/vkui";
import PropTypes from "prop-types";
import HardLevel from "../HardLevel/HardLevel";
import {useRouter} from "react-router5";
import {pages} from "../../router";
import {getImg} from "../../actions/functions";
import LazyImg from "../LazyImg/LazyImg";

const TrickElement = ({ id, name, level }) => {
  const router = useRouter();
  const goToTrick = () => router.navigate(pages.TRICK, {trickId: id});

  return (
    <Cell multiline onClick={() => {goToTrick(id)}}
          before={<LazyImg style={{width: '90%', borderRadius: '10%', display: 'flex', selfAlign: 'center'}} width={50} height={48} src={getImg(id)} />}
          description={<HardLevel level={level}/>}
    >{name}</Cell>
  );
};

TrickElement.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  level: PropTypes.oneOf([1,2,3,4]).isRequired,
};

export default TrickElement;
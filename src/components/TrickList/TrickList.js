import React, {useContext} from 'react';
import {Group, List} from "@vkontakte/vkui";
import TrickElement from "../TrickElement/TrickElement";
import Context from "../App/context";
import {inNames} from "../../actions/functions";

const TrickList = () => {
  const { activeHomeFilter, getTrickListByProgress, searchTrick } = useContext(Context);

  return (<Group>
    <List>
      {getTrickListByProgress(activeHomeFilter).map(({ img, name, alt_name, eng_name, level, id }) => (inNames(searchTrick, name, alt_name, eng_name) ? <TrickElement key={id} id={id} level={level} img={img} name={name}/> : ""))}
    </List>
  </Group>);
};


export default TrickList;